package co.adsicbi.portafolio.rest.services;

import co.adsicbi.portafolio.jpa.entities.Categorias;
import co.adsicbi.portafolio.jpa.sessions.CategoriasFacade;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author leoandresm
 */
@Path("categorias")
public class CategoriasRest {
    
    // Jersey will inject proxy of Security Context
    @Context
    SecurityContext securityContext;

    @EJB
    private CategoriasFacade ejbCategoriasFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Categorias categoria) {
        if (securityContext.isUserInRole("ADMIN")) {
              ejbCategoriasFacade.create(categoria);
              System.out.println("Creada categoria");
        }
    }

    @PUT
    @Path("{id}")
    @RolesAllowed("ADMIN")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, Categorias categoria) {
        if (securityContext.isUserInRole("ADMIN")) {
              ejbCategoriasFacade.edit(categoria);
              System.out.println("Editada categoria");
        }
  
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Short id) {
        ejbCategoriasFacade.remove(ejbCategoriasFacade.find(id));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorias> findAll() {
        return ejbCategoriasFacade.findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categorias findById(@PathParam("id") String id) {
        return ejbCategoriasFacade.find(id);
    }
}
