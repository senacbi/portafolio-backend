package co.adsicbi.portafolio.rest.services;

import co.adsicbi.portafolio.jpa.entities.Usuarios;
import co.adsicbi.portafolio.jpa.sessions.UsuariosFacade;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author leoandresm
 */
@Path("usuarios")
@Produces(MediaType.APPLICATION_JSON)
public class UsuariosRest {

    @EJB
    private UsuariosFacade ejbUsuariosFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Usuarios usuario) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
                if (ejbUsuariosFacade.findByEmail(usuario.getEmail()) == null) {
                //Validar si es mayor de edad
                Calendar now = Calendar.getInstance();//Obtener la fecha actual
                now.add(Calendar.YEAR, -18);//Restar 18 años
                if (usuario.getFechaNac().after(now.getTime())) {//Validar que la fecha de nacimiento no sea posterior
                    //Retorna error y detiene la creación del usuario si no cumple con la validación de fecha
                    return Response
                            .status(Response.Status.CONFLICT)
                            .entity(gson.toJson("Debe tener mas de 18 años para registrarse"))
                            .build();
                }
                ejbUsuariosFacade.create(usuario);//Crea el usuario en la base de datos
                return Response.ok()
                        .entity(gson.toJson("El usuario fue creado exitosamente"))
                        .build();
            } else {
                //Retornar error si no cumple con la validación del email
                return Response
                        .status(Response.Status.CONFLICT)
                        .entity(gson.toJson("El email ya esta registrado"))
                        .build();
            }
        } catch (EJBException ex) {
            String msg = "";
            Throwable cause = ex.getCause();
            if (cause != null) {
                if (cause instanceof ConstraintViolationException) {
                    ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;
                    for (ConstraintViolation<?> constraintViolation : constraintViolationException.getConstraintViolations()) {
                        msg += "{";
                        msg += "entity: " + constraintViolation.getLeafBean().toString() + ",";
                        msg += "field: " + constraintViolation.getPropertyPath().toString() + ",";
                        msg += "invalidValue: " + constraintViolation.getInvalidValue().toString() + ",";
                        msg += "error: " + constraintViolation.getMessage();
                        msg += "}";
                    }
                } else {
                    msg = cause.getLocalizedMessage();
                }
            }
            if (msg.length() > 0) {
                return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(msg)).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }

    }

    @PUT
    @RolesAllowed({"ADMIN","USER"})
    @Path("{id}")
    public void edit(@PathParam("id") Integer id, Usuarios usuario) {
        ejbUsuariosFacade.edit(usuario);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbUsuariosFacade.remove(ejbUsuariosFacade.find(id));
    }

    @GET
    @RolesAllowed("ADMIN")
    public List<Usuarios> findAll() {
        return ejbUsuariosFacade.findAll();
    }

    @GET
    @Path("{id}")
    public Usuarios findById(@PathParam("id") Integer id) {
        return ejbUsuariosFacade.find(id);
    }

    @GET
    @Path("nombre/{nombre}")
    public List<Usuarios> findByNombre(@PathParam("nombre") String nombre) {
        return ejbUsuariosFacade.findByNombre(nombre);
    }

}
