package co.adsicbi.portafolio.rest.services;

import co.adsicbi.portafolio.jpa.entities.Ciudades;
import co.adsicbi.portafolio.jpa.sessions.CiudadesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author leoandresm
 */
@Path("ciudades")
@Produces(MediaType.APPLICATION_JSON)
public class CiudadesRest {

    @EJB
    private CiudadesFacade ejbCiudadesFacade;

    @GET    
    public List<Ciudades> findAll() {
        return ejbCiudadesFacade.findAll();
    }
   
    @GET
    @Path("nombre/{query}")
    public List<Ciudades> findByNombre(@PathParam("query") String query){
        return ejbCiudadesFacade.findByNombre(query);
    }
}
