package co.adsicbi.portafolio.rest.services;

import co.adsicbi.portafolio.jpa.entities.CalificacionesServicio;
import co.adsicbi.portafolio.jpa.entities.Usuarios;
import co.adsicbi.portafolio.jpa.sessions.CalificacionesServicioFacade;
import co.adsicbi.portafolio.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author leoandresm
 */
@Path("calificaciones")
@Produces(MediaType.APPLICATION_JSON)
public class CalificacionesServicioREST {

    @EJB
    private CalificacionesServicioFacade ejbCalificacionesFacade;

    @Context
    private HttpServletRequest request;

    @Path("create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(CalificacionesServicio calificacion) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            calificacion.setIdUsuario(
                    new Usuarios(
                            Integer.parseInt(
                                    AuthUtils.getSubject(
                                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
            calificacion.setFecha("");
            ejbCalificacionesFacade.create(calificacion);
            return Response.ok().entity(gson.toJson("La calificacion fue registrada exitosamente")).build();
        } catch (ParseException | JOSEException | NumberFormatException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CalificacionesServicio> findAll() {
        return ejbCalificacionesFacade.findAll();
    }

}
