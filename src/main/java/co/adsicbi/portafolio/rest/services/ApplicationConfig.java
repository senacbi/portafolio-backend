package co.adsicbi.portafolio.rest.services;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author leoandresm
 */
@ApplicationPath("webresources")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("co.adsicbi.portafolio.rest.services;co.adsicbi.portafolio.rest.auth");
        register(RolesAllowedDynamicFeature.class);
    }
    

}
